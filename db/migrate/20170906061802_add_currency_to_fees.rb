class AddCurrencyToFees < ActiveRecord::Migration[5.1]
  def change
    add_belongs_to :fees, :from_currency, references: :currency, index: true
  end
end
