class AddIndexsToExchangeRates < ActiveRecord::Migration[5.1]
  def change
    remove_index :exchange_rates, [:from_id, :to_id]

    add_index :exchange_rates, [:from_id, :to_id, :bank_id, :recorded_at], name: 'exchange_rate_full_index'
    add_index :exchange_rates, [:recorded_at]
  end
end
