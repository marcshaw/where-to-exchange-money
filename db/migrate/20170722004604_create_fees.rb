class CreateFees < ActiveRecord::Migration[5.1]
  def change
    create_table :fees do |t|
      t.references :bank
      t.decimal :percentage,   null: false
      t.decimal :minimum,      null: false
      t.datetime :recorded_at, null: false
      t.timestamps
    end
  end
end
