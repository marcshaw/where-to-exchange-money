class CreateExchangeRates < ActiveRecord::Migration[5.1]
  def change
    create_table :exchange_rates do |t|
      t.references :bank, foreign_key: { to_table: :banks }
      t.references :from, foreign_key: { to_table: :currencies }
      t.references :to, foreign_key: { to_table: :currencies }
      t.decimal    :rate,        null: false, :precision => 10, :scale => 5
      t.datetime   :recorded_at, null: false
      t.timestamps
    end
  end
end
