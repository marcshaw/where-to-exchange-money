class AddFromToIndexOnExchangeRates < ActiveRecord::Migration[5.1]
  def change
    add_index(:exchange_rates, [:from_id, :to_id])
  end
end
