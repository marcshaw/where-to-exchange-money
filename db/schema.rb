# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170926050009) do

  create_table "banks", force: :cascade do |t|
    t.string "name", null: false
    t.string "url", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string "name", null: false
    t.string "code", null: false
    t.string "symbol", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exchange_rates", force: :cascade do |t|
    t.integer "bank_id"
    t.integer "from_id"
    t.integer "to_id"
    t.decimal "rate", precision: 10, scale: 5, null: false
    t.datetime "recorded_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bank_id"], name: "index_exchange_rates_on_bank_id"
    t.index ["from_id", "to_id", "bank_id", "recorded_at"], name: "exchange_rate_full_index"
    t.index ["from_id"], name: "index_exchange_rates_on_from_id"
    t.index ["recorded_at"], name: "index_exchange_rates_on_recorded_at"
    t.index ["to_id"], name: "index_exchange_rates_on_to_id"
  end

  create_table "fees", force: :cascade do |t|
    t.integer "bank_id"
    t.decimal "percentage", null: false
    t.decimal "minimum", null: false
    t.datetime "recorded_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "from_currency_id"
    t.index ["bank_id"], name: "index_fees_on_bank_id"
    t.index ["from_currency_id"], name: "index_fees_on_from_currency_id"
  end

end
