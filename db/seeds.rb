# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
ActiveRecord::Base.transaction do
  westpac = Bank.create!(name: "westpac", url: "https://www.westpac.co.nz/managing-your-money/resources/foreign-exchange-rates")
  anz = Bank.create!(name: "anz", url: "https://www.anz.co.nz/ratefee/forexchange.asp")
  asb = Bank.create!(name: "asb", url: "https://www.asb.co.nz/foreign-exchange/foreign-exchange-rates.html")
  bnz = Bank.create!(name: "bnz", url: "https://www.bnz.co.nz/personal-banking/international/exchange-rates")
  kiwibank = Bank.create!(name: "kiwibank", url: "https://www.kiwibank.co.nz/personal-banking/rates-and-fees/fx-rates/")

  Currency.create!(name: "New Zealand Dollar", code: "NZD", symbol: "$")
  Currency.create!(name: "Australian Dollar", code: "AUD", symbol: "$")
  Currency.create!(name: "Canadian Dollar", code: "CAD", symbol: "$")
  Currency.create!(name: "Chinese Yuan", code: "CNY", symbol: "¥")
  Currency.create!(name: "Swiss Franc", code: "CHF", symbol: "CHF")
  Currency.create!(name: "Euro", code: "EUR", symbol: "€")
  Currency.create!(name: "Fiji Dollar", code: "FJD", symbol: "$")
  Currency.create!(name: "British Pound", code: "GBP", symbol: "£")
  Currency.create!(name: "Korean Won", code: "KRW", symbol: "₩")
  Currency.create!(name: "Hong Kong Dollar", code: "HKD", symbol: "$")
  Currency.create!(name: "Indian Rupee", code: "INR", symbol: "₹")
  Currency.create!(name: "Singapore Dollar", code: "SGD", symbol: "$")
  Currency.create!(name: "Thai Bhat", code: "THB", symbol: "฿")
  Currency.create!(name: "United States Dollar", code: "USD", symbol: "$")
  Currency.create!(name: "Japanese Yen", code: "JPY", symbol: "¥")
  Currency.create!(name: "South African Rand", code: "ZAR", symbol: "R")
  Currency.create!(name: "Danish Krone", code: "DKK", symbol: "kr")
  Currency.create!(name: "Swedish Krona", code: "SEK", symbol: "kr")

  Currency.where.not(code: "NZD").each do |currency|
    Fee.create!(bank: westpac, percentage: 0.0, minimum: 5, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: anz, percentage: 0.0, minimum: 5, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: asb, percentage: 0.0, minimum: 0, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: bnz, percentage: 0.0, minimum: 5, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: kiwibank, percentage: 0.0, minimum: 0, recorded_at: Time.now, from_currency: currency)
  end

  Currency.where(code: "NZD").each do |currency|
    Fee.create!(bank: westpac, percentage: 1.0, minimum: 10, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: anz, percentage: 1.1, minimum: 12, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: asb, percentage: 1.0, minimum: 10, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: bnz, percentage: 1.0, minimum: 10, recorded_at: Time.now, from_currency: currency)
    Fee.create!(bank: kiwibank, percentage: 0.0, minimum: 10, recorded_at: Time.now, from_currency: currency)
  end
end
