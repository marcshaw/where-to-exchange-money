module Nzd
  module Commands
    class MapInterest
      attr_reader :parsed_data

      FROM_CURRENCY = "NZD"

      def initialize(parsed_data:)
        @parsed_data = parsed_data
      end

      def execute
        validate_data
        map_parsed_data
      rescue Exception => e
        log_data_on_error
        raise e
      end

      private

      def map_parsed_data
        parsed_data.flat_map do |bank_name, bank_data|
          map_bank_data(bank_name, bank_data)
        end
      end

      def map_bank_data(bank_name, bank_data)
        bank_data[:currencies].map do |currency_code, currency_data|
          { bank: bank_name, from_currency: FROM_CURRENCY, to_currency: currency_code.to_s, rate: currency_data[:rate], recorded_at: Time.now.change(:sec => 0) }
        end
      end

      def validate_data
        valid = Validation.new.valid_data?(parsed_data)

        if !valid
          log_data_on_error
          raise ArgumentError, 'Map Interest can not map the data'
        end
      end

      def log_data_on_error
        Rails.logger.error "Map Interest was not able to map the data given"
        Rails.logger.error parsed_data
      end

      class Validation
        def valid_data?(parsed_data)
          valid_top_level_hash?(parsed_data) &&
            valid_inner_structure?(parsed_data)
        end

        private

        def valid_inner_structure?(parsed_data)
          parsed_data.each do |key, value|
            return false unless value&.dig(:currencies).present?
          end
        end

        def valid_top_level_hash?(parsed_data)
          parsed_data.is_a?(Hash)
        end
      end
    end
  end
end
