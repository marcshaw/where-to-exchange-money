module Nzd
  module Commands
    class ParseKiwibank
      attr_reader :data

      CURRENCY = "NZD"
      BANK_NAME = "kiwibank"

      def initialize(data:)
        @data = data["results"]
      end

      def execute
        begin
          parse_data.compact
        rescue StandardError => e
          log_data_on_error
          raise e
        end
      end

      private

      def parse_data
        data.map do |currency|
          if currency['property2'] == "CASH"
            if currency['property3'] == "SELL"
              { bank: BANK_NAME, from_currency: CURRENCY, to_currency: currency['property1'], rate: currency['rate'], recorded_at: Time.now.change(:sec =>0) }
            elsif currency['property3'] == "BUY"
              { bank: BANK_NAME, from_currency: currency['property1'], to_currency: CURRENCY, rate: currency['rate'], recorded_at: Time.now.change(:sec =>0) }
            end
          end
        end
      end

      def log_data_on_error
        Rails.logger.error "Parse Kiwibank was not able to parse the data given"
        Rails.logger.error data
      end
    end
  end
end
