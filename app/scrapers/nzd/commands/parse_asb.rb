module Nzd
  module Commands
    class ParseAsb
      attr_reader :data

      CURRENCY = "NZD"
      BANK_NAME = "asb"

      def initialize(data:)
        @data = data
      end

      def execute
        begin
          parse_data
        rescue StandardError => e
          log_data_on_error
          raise e
        end
      end

      private

      def parse_data
        data["value"].map do |exchange|
          other_currency = exchange["currencyCode"]
          to_nzd = exchange["buysNotes"]
          from_nzd = exchange["sellsNotes"]
          [
            { bank: BANK_NAME, from_currency: CURRENCY, to_currency: other_currency, rate: from_nzd, recorded_at: Time.now.change(:sec =>0) },
            { bank: BANK_NAME, from_currency: other_currency, to_currency: CURRENCY, rate: to_nzd, recorded_at: Time.now.change(:sec =>0) }
          ]
        end.flatten
      end

      def log_data_on_error
        Rails.logger.error "Parse Kiwibank was not able to parse the data given"
        Rails.logger.error data
      end
    end
  end
end
