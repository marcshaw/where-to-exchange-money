module Nzd
  module Commands
    class ParseInterest
      attr_reader :data

      BANKS_SUPPORTED = ["westpac", "anz", "bnz", "asb"]

      def initialize(data:)
        @data = data["exchange_rates"]
      end

      def execute
        validate_data
        begin
          banks = initiate_banks
          parse_data(banks)
        rescue Exception => e
          log_data_on_error
          raise e
        end

        banks
      end

      private

      def validate_data
        if data&.dig(0)&.dig("data")&.dig(0)&.dig("value") != "Code"
          log_data_on_error
          raise ArgumentError, 'Parse Interest can not parse the data'
        end
      end

      def initiate_banks
        Hash.new.tap do |banks|
          data[0]["data"].each_with_index do |mapped_value, i|
            value = mapped_value["value"]

            if BANKS_SUPPORTED.include?(value.downcase)
              banks[value.downcase] = { :position => i, :currencies => {} }
            end
          end
        end
      end

      def parse_data(banks)
        currencies.each do |mapped_data|
          currency_data = dig_currency_data(mapped_data)
          extract_currency_data(banks, currency_data)
        end
      end

      def extract_currency_data(banks, currency_data)
        code = currency_data[0]
        currency_name = currency_data[1]

        banks.each do |k,v|
          rate = currency_data[v[:position]]
          v[:currencies][code.to_sym] = { :currency_name => currency_name, :rate => rate }
        end
      end

      def dig_currency_data(mapped_data)
        mapped_data["data"].map { |data| data["value"] }
      end

      def currencies
        data.slice(2..data.length - 4)
      end

      def log_data_on_error
        Rails.logger.error "Parse Interest was not able to parse the data given"
        Rails.logger.error data
      end
    end
  end
end
