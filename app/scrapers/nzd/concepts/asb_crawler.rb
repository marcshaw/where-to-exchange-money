module Nzd
  module Concepts
    class AsbCrawler
      def scrape
        request = HTTP.headers(:apikey => "l7xx93b1538ae6564c3fa170f899b645c605")
                    .get("https://api.asb.co.nz/public/v1/exchange-rates")

        JSON.parse(request.to_s)
      end
    end
  end
end
