module Nzd
  module Concepts
    class WestpacWombatCrawler
      include Wombat::Crawler

      base_url "https://www.westpac.co.nz"
      path "/managing-your-money/resources/foreign-exchange-rates"

      exchange_rates 'css=.table-data-sheet', :iterator do
        data "xpath=./tbody/tr", :iterator do
          data "xpath=./td", :iterator do
            value "xpath=."
          end
        end
      end

      def scrape
        crawl
      end
    end
  end
end
