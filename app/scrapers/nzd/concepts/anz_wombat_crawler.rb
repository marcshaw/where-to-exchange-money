module Nzd
  module Concepts
    class AnzWombatCrawler
      include Wombat::Crawler

      base_url "https://www.anz.co.nz"
      path "/ratefee/forexchange.asp"

      exchange_rates "xpath=//*[@id='fxtable']/tbody/tr", :iterator do
        data 'xpath=./td', :iterator do
          name 'xpath=.'
        end
      end

      def scrape
        crawl
      end
    end
  end
end
