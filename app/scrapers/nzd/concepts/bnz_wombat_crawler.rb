module Nzd
  module Concepts
    class BnzWombatCrawler
      include Wombat::Crawler

      base_url "https://www.bnz.co.nz"
      path "/XMLFeed/portal/fx/xml"
      document_format :xml

      exchange_rates "xpath=/rss/standard/rate", :iterator do
        currency 'xpath=currency'
        notebuy 'xpath=notebuy'
        notesell 'xpath=notesell'
      end

      def scrape
        crawl
      end
    end
  end
end
