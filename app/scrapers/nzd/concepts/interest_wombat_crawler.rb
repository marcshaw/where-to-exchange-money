module Nzd
  module Concepts
    class InterestWombatCrawler
      include Wombat::Crawler

      base_url "https://www.interest.co.nz"
      path "/Calculators/buying-foreign-currency"

      exchange_rates "xpath=/html/body/div/div[1]/div/div[1]/article/div/table/tbody/tr", :iterator do
        data 'xpath=./td', :iterator do
          value 'xpath=.'
        end
      end

      def scrape
        crawl
      end
    end
  end
end
