module Nzd
  module Facades
    class ScrapeWestpac
      def execute
        scraped_data = ::Commands::ScrapeSite.new(scraper: Nzd::Concepts::WestpacWombatCrawler).execute
        data = Nzd::Commands::ParseWestpac.new(data: scraped_data).execute
        ::Commands::CreateExchangeRates.new(mapped_data: data).execute
      end
    end
  end
end
