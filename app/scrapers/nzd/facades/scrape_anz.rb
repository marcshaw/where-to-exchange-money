module Nzd
  module Facades
    class ScrapeAnz
      def execute
        scraped_data = ::Commands::ScrapeSite.new(scraper: Nzd::Concepts::AnzWombatCrawler).execute
        data = Nzd::Commands::ParseAnz.new(data: scraped_data).execute
        ::Commands::CreateExchangeRates.new(mapped_data: data).execute
      end
    end
  end
end
