module Nzd
  module Facades
    class ScrapeSites
      def execute
        ExchangeRate.transaction do
          Nzd::Facades::ScrapeAsb.new.execute
          Nzd::Facades::ScrapeAnz.new.execute
          Nzd::Facades::ScrapeWestpac.new.execute
          Nzd::Facades::ScrapeBnz.new.execute
          Nzd::Facades::ScrapeKiwibank.new.execute
        end
      end
    end
  end
end
