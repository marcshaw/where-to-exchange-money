module Nzd
  module Facades
    class ScrapeInterest
      def execute
        scraped_data = ::Commands::ScrapeSite.new(scraper: Nzd::Concepts::InterestWombatCrawler.new).execute
        parsed_data = Nzd::Commands::ParseInterest.new(data: scraped_data).execute
        mapped_data = Nzd::Commands::MapInterest.new(parsed_data: parsed_data).execute
        ::Commands::CreateExchangeRates.new(mapped_data: mapped_data).execute
      end
    end
  end
end
