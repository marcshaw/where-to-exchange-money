module Nzd
  module Facades
    class ScrapeBnz
      def execute
        scraped_data = ::Commands::ScrapeSite.new(scraper: Nzd::Concepts::BnzWombatCrawler).execute
        data = Nzd::Commands::ParseBnz.new(data: scraped_data).execute
        ::Commands::CreateExchangeRates.new(mapped_data: data).execute
      end
    end
  end
end
