module Nzd
  module Facades
    class ScrapeKiwibank
      def execute
        scraped_data = ::Commands::ScrapeSite.new(scraper: Nzd::Concepts::KiwibankCrawler).execute
        data = Nzd::Commands::ParseKiwibank.new(data: scraped_data).execute
        ::Commands::CreateExchangeRates.new(mapped_data: data).execute
      end
    end
  end
end
