module Nzd
  module Facades
    class ScrapeAsb
      def execute
        scraped_data = ::Commands::ScrapeSite.new(scraper: Nzd::Concepts::AsbCrawler).execute
        data = Nzd::Commands::ParseAsb.new(data: scraped_data).execute
        ::Commands::CreateExchangeRates.new(mapped_data: data).execute
      end
    end
  end
end
