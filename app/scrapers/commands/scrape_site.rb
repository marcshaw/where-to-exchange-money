  module Commands
    class ScrapeSite
      attr_reader :scraper

      def initialize(scraper:)
        @scraper = scraper
      end

      def execute
        scraper.new.scrape
      end
    end
  end
