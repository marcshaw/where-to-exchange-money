module Commands
  class CreateExchangeRates
    attr_reader :mapped_data, :create_exchange_rate

    AssociatedExchangeRate = Struct.new(
      :bank,
      :to_currency,
      :from_currency,
      :rate,
      :recorded_at
    )

    def initialize(mapped_data:, create_exchange_rate: Commands::CreateExchangeRate)
      @mapped_data = mapped_data
      @create_exchange_rate = create_exchange_rate
    end

    def execute
      @mapped_data.each do |data|
        associated_data = find_data_records(data)

        if valid_data?(associated_data)
          exchange_rate = save_exchange_rate(associated_data)
          log_error(exchange_rate.errors.full_messages) if exchange_rate.invalid?
        else
          log_error(data)
        end
      end
    end

    private

    def valid_data?(data)
      data.bank.present? &&
        data.to_currency.present? &&
        data.from_currency.present? &&
        data.rate.present? &&
        data.recorded_at.present?
    end

    def find_data_records(data)
      AssociatedExchangeRate.new(
        Bank.find_by(name: data[:bank]),
        Currency.find_by(code: data[:to_currency]),
        Currency.find_by(code: data[:from_currency]),
        data[:rate],
        data[:recorded_at],
      )
    end

    def save_exchange_rate(data)
      create_exchange_rate.new(
        bank: data.bank,
        to_currency: data.to_currency,
        from_currency: data.from_currency,
        rate: data.rate,
        recorded_at: data.recorded_at,
      ).execute
    end

    def log_error(error)
      Rails.logger.error "Exchange failed to be created"
      Rails.logger.error error
    end
  end
end
