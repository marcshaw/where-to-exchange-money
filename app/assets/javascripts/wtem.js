// The entire application is namespaced behind this Wtem variable
var Wtem = function () {
  /*
    =================================================================
    APP VARIABLES
    =================================================================
  */

  // view element selectors
  var wtemSearchSelector = '.wtem-search',
      wtemLoadingSelector = '.wtem-loading',
      wtemFormSelector = '.wtem-search > form',
      wtemResultsSelector = '.wtem-results',
      amountFormSelector = '.wtem-form__amount > .wtem-input__group > input',
      fromCurrencyFormSelector = '.wtem-form__from > select',
      toCurrencyFormSelector = '.wtem-form__to > select',
      currencySwitcherButtonSelector = '.wtem-form__switch > button',
      exchangeRateDealsViewSelector = '.wtem-exchange-rate-deals';

  // view elements
  var wtemSearch = null,
      wtemForm = null,
      wtemResults = null,
      amountInput = null,
      toCurrencySelect = null,
      fromCurrencySelect = null,
      currencySwitcherButton = null,
      exchangeRateDealsView = null;

  //  models
  var exchangeRates = [],
      currencies = [],
      banks = [],
      fees = [];

  // the amount to exchange
  var fromAmount;

  // the base currency's id
  var fromCurrencyId;

  // the target currency's id
  var toCurrencyId;

  // the results
  var exchangeRateDeals = [];

  // services
  var getWtemData = new GetWtemData(),
      parseFees = new ParseFees(),
      parseCurrencies = new ParseCurrencies(),
      parseBanks = new ParseBanks(),
      parseExchangeRates = new ParseExchangeRates(),
      calculateExchangeRateDeals = new CalculateExchangeRateDeals(),
      updateExchangeRateDealsView = new UpdateExchangeRateDealsView(),
      updateExchangeRateInputView = new UpdateExchangeRateInputView(),
      updateSelectedCurrencies= new UpdateSelectedCurrencies();

  /*
    =================================================================
    APP INTERACTION
    =================================================================
  */

  var initializeWtemUI = function () {
    wtemForm.addEventListener('submit', function(e){ e.preventDefault(); });
    amountInput.addEventListener('keyup', updateExchangeRateDeals);
    fromCurrencySelect.addEventListener('change', updateToCurrencyOptionsAndExchangeRateDeals);
    toCurrencySelect.addEventListener('change', updateExchangeRateDeals);
    currencySwitcherButton.addEventListener('click', switchFromAndToRates);

    var yearCopy = $('#year-copy'), d = new Date();

    if (d.getFullYear() === 2017) {
      yearCopy.html('2017');
    } else {
      yearCopy.html('2017-' + d.getFullYear().toString().substr(2,2));
    }
  };

  var updateExchangeRateDeals = function () {
    fromAmount = parseFloat(amountInput.value.replace(/,/g,'').substr(1));
    fromCurrencyId = parseFloat(fromCurrencySelect.options[fromCurrencySelect.selectedIndex].value);
    toCurrencyId = parseFloat(toCurrencySelect.options[toCurrencySelect.selectedIndex].value);

    exchangeRateDeals = calculateExchangeRateDeals.execute(exchangeRates, fromAmount, fromCurrencyId, toCurrencyId);
    updateExchangeRateDealsView.execute(exchangeRateDeals, exchangeRateDealsView);
    updateExchangeRateInputView.execute(fromAmount, fromCurrencyId, toCurrencyId, exchangeRateDeals, exchangeRates, wtemSearch);
  };

  var updateToCurrencyOptionsAndExchangeRateDeals = function () {
    var newFromCurrencyId = parseFloat(fromCurrencySelect.options[fromCurrencySelect.selectedIndex].value),
        newToCurrencyId   = null;

    updateSelectedCurrencies.execute(exchangeRates, currencies, newFromCurrencyId, newToCurrencyId, fromCurrencySelect, toCurrencySelect);

    updateExchangeRateDeals();
  };

  var switchFromAndToRates = function () {
    var newFromCurrencyId = toCurrencyId,
        newToCurrencyId   = fromCurrencyId;

    updateSelectedCurrencies.execute(exchangeRates, currencies, newFromCurrencyId, newToCurrencyId, fromCurrencySelect, toCurrencySelect);

    updateExchangeRateDeals();
  };

  var initializeWtemSuccess = function (response) {
    fees          = parseFees.execute(response);
    currencies    = parseCurrencies.execute(response);
    banks         = parseBanks.execute(response, fees);
    exchangeRates = parseExchangeRates.execute(response, currencies, banks);

    initializeWtemUI();
    removeLoading();
    updateExchangeRateDeals();
  };

  var initializeWtemError = function (error) {
    // TODO show a nice error screen
    console.log(error);
  };

  var removeLoading = function() {
    var loadingElement = wtemSearch.querySelector(wtemLoadingSelector);

    wtemSearch.removeChild(loadingElement);
    wtemResults.classList.remove('wtem-results--loading');
  };

  /*
    =================================================================
    APP INITIALISATION
    =================================================================
  */

  var initializeWtem = function () {
    wtemSearch             = document.querySelector(wtemSearchSelector);
    wtemForm               = document.querySelector(wtemFormSelector);
    wtemResults            = document.querySelector(wtemResultsSelector);
    exchangeRateDealsView  = document.querySelector(exchangeRateDealsViewSelector);

    amountInput            = wtemForm.querySelector(amountFormSelector);
    fromCurrencySelect     = wtemForm.querySelector(fromCurrencyFormSelector);
    toCurrencySelect       = wtemForm.querySelector(toCurrencyFormSelector);
    currencySwitcherButton = wtemForm.querySelector(currencySwitcherButtonSelector);

    getWtemData.execute(initializeWtemSuccess, initializeWtemError);
  };

  return {
    init: function () {
      initializeWtem();
    }
  };
}();

/* Initialise app when page loads */
$(function () {
  Wtem.init();
});
