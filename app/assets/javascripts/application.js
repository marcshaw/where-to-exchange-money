// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require isotope.pkgd.min
//= require numeral.min
//= require autonumeric
//
//= require models/bank
//= require models/currency
//= require models/exchange_rate
//= require models/exchange_rate_deal
//= require models/fee
//= require presenters/exchange_rate_deal_presenter
//= require presenters/exchange_rate_deals_presenter
//= require services/get_wtem_data
//= require services/parse_fees
//= require services/parse_currencies
//= require services/parse_banks
//= require services/parse_exchange_rates
//= require services/find_exchange_rates
//= require services/calculate_exchange_rates_deals
//= require services/update_exchange_rate_deals_view
//= require services/update_exchange_rate_input_view
//= require services/update_selected_currencies
//= require wtem
