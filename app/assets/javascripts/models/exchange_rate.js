function ExchangeRate(id, bank, fromCurrency, toCurrency, rate, fee) {
  this.id = id;
  this.bank = bank;
  this.fromCurrency = fromCurrency;
  this.toCurrency = toCurrency;
  this.rate = rate;
  this.fee = fee;
}
