function ExchangeRateDeal(exchangeRate, fromAmount) {
  this.exchangeRate = exchangeRate;
  this.fromAmount = (fromAmount || 0);
  this.feeAmount = this._calculateFeeAmount(exchangeRate, this.fromAmount);
  this.deal = this._calculateExchangeRateDeal(exchangeRate, this.fromAmount, this.feeAmount);
}

ExchangeRateDeal.prototype._calculateFeeAmount = function (exchangeRate, fromAmount) {
  // fetch minimum fee amount
  var feeMinimumAmount = exchangeRate.fee.minimum;

  // calculate fee percentage amount
  var feePercentageAmount = fromAmount * (exchangeRate.fee.percentage / 100);

  // return fee amount
  return Math.max(feeMinimumAmount, feePercentageAmount);
};

ExchangeRateDeal.prototype._calculateExchangeRateDeal = function (exchangeRate, fromAmount, feeAmount) {
  // apply fees

  // calculate remaining to currency amount
  //When we add aussie banks -- we will need to add something onto the model itself
  //along the lines of the home currency for a bank
  if(exchangeRate.toCurrency.code == "NZD"){
    var toAmount = (fromAmount / exchangeRate.rate) - feeAmount;
  } else {
    var amountMinusFees = fromAmount - feeAmount;
    var toAmount = amountMinusFees * exchangeRate.rate;
  }

  // round to nearest two decimal places
  return parseFloat(toAmount.toFixed(2));
};
