function Fee(id, bankId, percentage, minimum, fromCurrencyId) {
  this.id = id;
  this.bankId = bankId;
  this.percentage = percentage;
  this.minimum = minimum;
  this.fromCurrencyId = fromCurrencyId;
}
