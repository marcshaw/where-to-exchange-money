function ExchangeRateDealPresenter(exchangeRateDeal, bestExchangeRateDeal) {
  this.bankId              = this._presentBankId(exchangeRateDeal);
  this.rawDeal             = this._presentRawDeal(exchangeRateDeal);
  this.deal                = this._presentDeal(exchangeRateDeal);
  this.rateFees            = this._presentRateFees(exchangeRateDeal);
  this.rateFeesExplanation = this._presentRateFeesExplanation(exchangeRateDeal);
  this.remark              = this._presentRemark(exchangeRateDeal, bestExchangeRateDeal);
  this.isBestDeal          = this._presentIsBestDeal(exchangeRateDeal, bestExchangeRateDeal);
}

ExchangeRateDealPresenter.prototype._presentBankId = function (exchangeRateDeal) {
  return exchangeRateDeal.exchangeRate.bank.id;
};

ExchangeRateDealPresenter.prototype._presentRawDeal = function (exchangeRateDeal) {
  return exchangeRateDeal.deal;
};

ExchangeRateDealPresenter.prototype._presentDeal = function (exchangeRateDeal) {
  var toCurrency = exchangeRateDeal.exchangeRate.toCurrency,
      dealStr = exchangeRateDeal.deal,
      deal = parseFloat(dealStr).toFixed(2);

  if(deal <= 0) {
    return this._toCurrencyStr(toCurrency, 0);
  } else {
    return this._toCurrencyStr(toCurrency, deal);
  }
};

ExchangeRateDealPresenter.prototype._presentRateFees = function (exchangeRateDeal) {
  var fromCurrency = exchangeRateDeal.exchangeRate.fromCurrency,
      toCurrency   = exchangeRateDeal.exchangeRate.toCurrency,
      rate         = exchangeRateDeal.exchangeRate.rate,
      feeAmount    = exchangeRateDeal.feeAmount;

  if(toCurrency.code == "NZD"){
    var displayRate = 1 / rate;
  } else {
    var displayRate = 1 * rate;
  }

  return fromCurrency.code + "/" + toCurrency.code + ": " + fromCurrency.symbol + "1 = " + toCurrency.symbol + displayRate.toFixed(4) + ". Fee ($" + numeral(feeAmount).format('0,0[.]00') + ")";
};

ExchangeRateDealPresenter.prototype._presentRateFeesExplanation = function (exchangeRateDeal) {
  var percentage   = exchangeRateDeal.exchangeRate.fee.percentage,
      minimum      = exchangeRateDeal.exchangeRate.fee.minimum,
      fromCurrency = exchangeRateDeal.exchangeRate.fromCurrency.code;

  return "percentage: " + percentage + "% or minimum: " +  this._toCurrencyStr(fromCurrency, minimum) + ", whichever is the largest";
};

ExchangeRateDealPresenter.prototype._presentIsBestDeal = function (exchangeRateDeal, bestExchangeRateDeal) {
  var deal       = exchangeRateDeal.deal,
      bestDeal   = bestExchangeRateDeal.deal;

  return deal === bestDeal;
};

ExchangeRateDealPresenter.prototype._presentRemark = function (exchangeRateDeal, bestExchangeRateDeal) {
  var deal       = exchangeRateDeal.deal,
      bestDeal   = bestExchangeRateDeal.deal,
      toCurrency = exchangeRateDeal.exchangeRate.toCurrency;

  if(deal <= 0) {
    return "";
  }

  if(deal === bestDeal) {
    return "best deal";
  } else {
    return this._toCurrencyStr(toCurrency, (bestDeal - deal)) + " less than the best deal";
  }
};

ExchangeRateDealPresenter.prototype._toCurrencyStr = function (currency, value) {
  return currency.symbol + numeral(value).format('0,0[.]00');
};
