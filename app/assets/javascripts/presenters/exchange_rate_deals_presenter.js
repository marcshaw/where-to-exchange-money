function ExchangeRateDealsPresenter() {}

ExchangeRateDealsPresenter.prototype.wrapExchangeRateDeals = function (exchangeRateDeals) {
  var sortedExchangeRateDeals = this._sortExchangeRateDeals(exchangeRateDeals);
  var bestExchangeRateDeal = sortedExchangeRateDeals[0];

  return sortedExchangeRateDeals.map(function(exchangeRateDeal){
    return new ExchangeRateDealPresenter(exchangeRateDeal, bestExchangeRateDeal);
  });
};


ExchangeRateDealsPresenter.prototype._sortExchangeRateDeals = function (exchangeRateDeals) {
  return exchangeRateDeals.sort(function(a, b) {
    return Math.sign(b.deal - a.deal);
  });
};
