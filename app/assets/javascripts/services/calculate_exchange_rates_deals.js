function CalculateExchangeRateDeals() {}

CalculateExchangeRateDeals.prototype.execute = function (exchangeRates, fromAmount, fromCurrencyId, toCurrencyId) {
  var candidateExchangeRates = this._findCandidateExchangeRates(exchangeRates, fromCurrencyId, toCurrencyId);
  var exchangeRateDeals = this._calculateExchangeRateDeals(candidateExchangeRates, fromAmount);

  return exchangeRateDeals;
};

CalculateExchangeRateDeals.prototype._findCandidateExchangeRates = function (exchangeRates, fromCurrencyId, toCurrencyId) {
  var findExchangeRates = new FindExchangeRates();

  return findExchangeRates.execute(exchangeRates, fromCurrencyId, toCurrencyId);
};

CalculateExchangeRateDeals.prototype._calculateExchangeRateDeals = function (candidateExchangeRates, fromAmount) {
  return candidateExchangeRates.map(function(exchangeRate){
    return new ExchangeRateDeal(exchangeRate, fromAmount);
  });
};
