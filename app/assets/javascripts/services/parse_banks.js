function ParseBanks() {}

ParseBanks.prototype.execute = function (json, fees) {
  return this._parseBanks(json['banks'], fees);
};


ParseBanks.prototype._parseBanks = function (json, fees) {
  var self = this;

  return json.map(function(bankJson) {
    var id = bankJson['id'];
    var name = bankJson['name'];
    var bank_fees = self._only(fees, id, 'bankId');

    return new Bank(id, name, bank_fees);
  });
};

// TODO this is pretty brittle
ParseBanks.prototype._only = function (array, id, selector) {
  var results = array.filter(function(element) { return element[selector] === id; });

  return results;
};
