function FindExchangeRates() {
}

FindExchangeRates.prototype.execute = function (exchangeRates, fromCurrencyId, toCurrencyId) {
  return exchangeRates.filter(function(exchangeRate){
    return exchangeRate.fromCurrency.id === fromCurrencyId && exchangeRate.toCurrency.id === toCurrencyId;
  });
};
