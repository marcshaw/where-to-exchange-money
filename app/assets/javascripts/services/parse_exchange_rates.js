function ParseExchangeRates() {}

ParseExchangeRates.prototype.execute = function (json, currencies, banks) {
  return this._parseExchangeRates(json['exchange_rates'], currencies, banks);
};

ParseExchangeRates.prototype._parseExchangeRates = function (json, currencies, banks) {
  var self = this;

  return json.map(function(exchangeRateJson) {
    var id = exchangeRateJson['id'];
    var bankId = exchangeRateJson['bank_id'];
    var fromId = exchangeRateJson['from_id'];
    var toId = exchangeRateJson['to_id'];
    var rate = parseFloat(exchangeRateJson['rate']);

    var bank = self._only(banks, bankId, 'id');
    var fromCurrency = self._only(currencies, fromId, 'id');
    var toCurrency = self._only(currencies, toId, 'id');

    var fee = self._only(bank.fees, fromId, 'fromCurrencyId');

    return new ExchangeRate(id, bank, fromCurrency, toCurrency, rate, fee);
  });
};

// TODO this is pretty brittle
ParseExchangeRates.prototype._only = function (array, id, selector) {
  var results = array.filter(function(element) { return element[selector] === id; });

  return results[0];
};
