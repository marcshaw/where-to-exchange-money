function GetWtemData() {
  this.ACTION_PATH = '/api/v1/exchange_rates';
  this.HTTP_VERB = 'GET';
}

GetWtemData.prototype.execute = function (onSuccess, onError) {
  var request = new XMLHttpRequest();
  request.open(this.HTTP_VERB, this.ACTION_PATH, true);

  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      onSuccess(JSON.parse(this.response));
    } else {
      onError("There was a server error of some sort");
    }
  };

  request.onerror = function() {
    onError("There was a connection error of some sort");
  };

  request.send();
};
