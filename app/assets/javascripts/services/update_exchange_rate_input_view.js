function UpdateExchangeRateInputView() {
  this.FROM_AMOUNT_SELECTOR = '.wtem-form__amount';
  this.AMOUNT_SYMBOL_SELECTOR = '#amount';
  this.INPUT_MESSAGE_SELECTOR = '.wtem-search > .wtem-message__content';
  this.INPUT_MESSAGE_P_SELECTOR = '.wtem-search > .wtem-message__content > p';
  this.CURRENCY_SWITCHER_SELECTOR = '.wtem-search > .wtem-search__form > .wtem-form__switch > button';
}

UpdateExchangeRateInputView.prototype.execute = function (fromAmount, fromCurrencyId, toCurrencyId, exchangeRateDeals, exchangeRates, inputView) {
  this._updateExchangeRateDealsMessage(fromAmount, exchangeRateDeals, inputView);
  this._updateExchangeRateDealsSwitcher(fromCurrencyId, toCurrencyId, exchangeRates, inputView);
};

UpdateExchangeRateInputView.prototype._updateExchangeRateDealsMessage = function (fromAmount, exchangeRateDeals, inputView) {
  if(exchangeRateDeals.length > 0) {
    this._updateFromCurrencySymbol(exchangeRateDeals, inputView);

    if(!isNaN(fromAmount)) {
      if(!this._isAmountTooLow(exchangeRateDeals)){
        this._hideMessage(inputView);
      } else {
        this._showMessage("Please enter a higher amount.", inputView);
      }
    } else {
      this._showMessage("Please enter a valid amount.", inputView);
    }
  } else {
    this._showMessage("Oh no, we don't have any rates for this combination. :(", inputView);
  }
};

UpdateExchangeRateInputView.prototype._updateExchangeRateDealsSwitcher = function (fromCurrencyId, toCurrencyId, exchangeRates, inputView) {
  var findExchangeRates     = new FindExchangeRates(),
      switchedExchangeRates  = findExchangeRates.execute(exchangeRates, toCurrencyId, fromCurrencyId),
      switcherButtonElement = inputView.querySelector(this.CURRENCY_SWITCHER_SELECTOR);

  switcherButtonElement.disabled = !(switchedExchangeRates.length > 0);
};

UpdateExchangeRateInputView.prototype._updateFromCurrencySymbol = function (exchangeRateDeals, inputView) {
  var fromSymbol = exchangeRateDeals[0].exchangeRate.fromCurrency.symbol;
  var fromAmountElement = inputView.querySelector(this.AMOUNT_SYMBOL_SELECTOR)
  json = JSON.parse(fromAmountElement.attributes['data-autonumeric'].value);
  json['aSign'] = fromSymbol;
  $(fromAmountElement).autoNumeric('update', json);
};

UpdateExchangeRateInputView.prototype._showMessage = function(message, inputView) {
  var inputMessageElement = inputView.querySelector(this.INPUT_MESSAGE_SELECTOR),
      inputMessagePElement = inputView.querySelector(this.INPUT_MESSAGE_P_SELECTOR);

  this._updateElementContent(inputMessagePElement, message);
  inputMessageElement.classList.remove('wtem-message__content--hidden');
  inputMessageElement.classList.add('wtem-message__content--show');
};


UpdateExchangeRateInputView.prototype._hideMessage = function(inputView) {
  var inputMessageElement = inputView.querySelector(this.INPUT_MESSAGE_SELECTOR);

  inputMessageElement.classList.remove('wtem-message__content--show');
  inputMessageElement.classList.add('wtem-message__content--hidden');
};

UpdateExchangeRateInputView.prototype._updateElementContent = function(element, value) {
  element.innerHTML = value;
};

UpdateExchangeRateInputView.prototype._isAmountTooLow = function(exchangeRateDeals) {
  return exchangeRateDeals.every(function(exchangeRateDeal){
    return exchangeRateDeal.deal <= 0;
  });
};
