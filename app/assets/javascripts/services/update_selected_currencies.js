function UpdateSelectedCurrencies() {}

UpdateSelectedCurrencies.prototype.execute = function (exchangeRates, currencies, newFromCurrencyId, newToCurrencyId, fromCurrencySelect, toCurrencySelect) {
  var newToCurrencyOptions = this._buildNewToCurrencyOptions(exchangeRates, newFromCurrencyId, newToCurrencyId);

  // update selected fromCurrency
  var newFromCurrencyIndex = this._findOptionIndexWithValue(fromCurrencySelect.options, newFromCurrencyId);
  fromCurrencySelect.selectedIndex = newFromCurrencyIndex;

  toCurrencySelect.options.length = 0; // clear out existing options
  newToCurrencyOptions.forEach(function(option) {
    toCurrencySelect.options.add(option);
  });
};

UpdateSelectedCurrencies.prototype._buildNewToCurrencyOptions = function (exchangeRates, fromCurrencyId, toCurrencyId) {
  var selectableToCurrencies = this._findSelectableToCurrencies(exchangeRates, fromCurrencyId);
  var newToCurrencyIndex = null;

  if(toCurrencyId === null) {
    newToCurrencyIndex = 0;
  } else {
    newToCurrencyIndex = this._findExchangeRateIndexWithId(selectableToCurrencies, toCurrencyId);
  }

  var newToCurrencyOptions = [];
  for (var i = 0; i < selectableToCurrencies.length; i++) {
    var toCurrency = selectableToCurrencies[i],
        isSelected = (i === newToCurrencyIndex),
        option     = new Option(toCurrency.code, toCurrency.id, false, isSelected);

    newToCurrencyOptions.push(option);
  }

  return newToCurrencyOptions;
};

UpdateSelectedCurrencies.prototype._findSelectableToCurrencies = function (exchangeRates, fromCurrencyId) {
  var candidateExchangeRates = exchangeRates.filter(function(exchangeRate){
    return exchangeRate.fromCurrency.id === fromCurrencyId;
  });

  var selectableToCurrencies = candidateExchangeRates.map(function(exchangeRate) {
    return exchangeRate.toCurrency;
  });

  var uniqueSelectableToCurrencies = selectableToCurrencies.filter(function(toCurrency, index) {
    return selectableToCurrencies.indexOf(toCurrency) === index;
  });

  if(uniqueSelectableToCurrencies.length > 1) {
    return uniqueSelectableToCurrencies.sort(function (a, b) {
      return (a.code).localeCompare(b.code);
    });
  } else {
    return uniqueSelectableToCurrencies;
  }
};

UpdateSelectedCurrencies.prototype._findOptionIndexWithValue = function (options, value) {
  for (var i = 0; i < options.length; i++) {
    var optionId = parseInt(options[i].value);
    if (optionId === value) {
      return i;
    }
  }

  // fallback
  return 0;
};

UpdateSelectedCurrencies.prototype._findExchangeRateIndexWithId = function (exchangeRates, id) {
  var exchangeRateWithValue = exchangeRates.find(function(exchangeRate) {
    return exchangeRate.id === id;
  });

  return exchangeRates.indexOf(exchangeRateWithValue);
};
