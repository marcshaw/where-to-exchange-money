function UpdateExchangeRateDealsView() {
  this.BANK_ROW_SELECTOR              = '.wtem-bank';
  this.RATE_FEES_SELECTOR             = '.wtem-bank__rate-fees';
  this.RATE_FEES_EXPLANATION_SELECTOR = '.wtem-bank__rate-fees-explanation';
  this.RAW_DEAL_SELECTOR              = '.wtem-bank__raw-deal';
  this.DEAL_SELECTOR                  = '.wtem-bank__deal';
  this.REMARK_SELECTOR                = '.wtem-bank__remark';

  this.ISOTOPE_OPTIONS = {
    itemSelector: this.BANK_ROW_SELECTOR,
    layoutMode: 'vertical',
    getSortData: {
      deal: this._sortByDeal,
      name: '[data-name]'
    },
    sortAscending: {
      deal: false,
      name: true
    },
    sortBy: [ 'deal', 'name' ],
    filter: ':not(.no-rate)'
  };

  this._exchangeRateDealsPresenter = new ExchangeRateDealsPresenter();
}

UpdateExchangeRateDealsView.prototype.execute = function (exchangeRateDeals, exchangeRateDealsView) {
  var bankRowElements = exchangeRateDealsView.querySelectorAll(this.BANK_ROW_SELECTOR);
  var exchangeRateDealPresenters = this._exchangeRateDealsPresenter.wrapExchangeRateDeals(exchangeRateDeals);

  this._updateBankRowElements(bankRowElements, exchangeRateDealPresenters);

  var animatableExchangeRateDealsView = $(exchangeRateDealsView).isotope(this.ISOTOPE_OPTIONS);

  // force update
  animatableExchangeRateDealsView.isotope('updateSortData').isotope();
};

UpdateExchangeRateDealsView.prototype._updateBankRowElements = function(bankRowElements, exchangeRateDealPresenters) {
  var self = this;

  bankRowElements.forEach(function(bankRowElement) {
    var bankIdStr = bankRowElement.getAttribute('data-id'),
        bankId = parseInt(bankIdStr);

    self._updateBankRowElement(bankId, bankRowElement, exchangeRateDealPresenters);
  });
};

UpdateExchangeRateDealsView.prototype._updateBankRowElement = function(bankId, bankRowElement, exchangeRateDealPresenters) {
  var exchangeRateDealPresenter = this._findAssociatedExchangeRateDealPresenter(bankId, exchangeRateDealPresenters);

  if(exchangeRateDealPresenter) {
    this._updateBankRowElementWithPresenter(bankRowElement, exchangeRateDealPresenter);
  } else {
    this._updateBankRowElementToHide(bankRowElement);
  }
};

UpdateExchangeRateDealsView.prototype._findAssociatedExchangeRateDealPresenter = function(bankId, exchangeRateDealPresenters) {
  return exchangeRateDealPresenters.find(function(exchangeRateDeal) {
    return exchangeRateDeal.bankId === bankId;
  });
};

UpdateExchangeRateDealsView.prototype._updateBankRowElementWithPresenter = function(bankRowElement, exchangeRateDealPresenter) {
  var rateFeesElement            = bankRowElement.querySelector(this.RATE_FEES_SELECTOR),
      dealElement                = bankRowElement.querySelector(this.DEAL_SELECTOR),
      remarkElement              = bankRowElement.querySelector(this.REMARK_SELECTOR);

  if(exchangeRateDealPresenter.isBestDeal && exchangeRateDealPresenter.rawDeal > 0) {
    bankRowElement.classList.add('best-deal');
  } else {
    bankRowElement.classList.remove('best-deal');
  }

  bankRowElement.classList.remove('no-rate');
  bankRowElement.setAttribute('data-deal', exchangeRateDealPresenter.rawDeal);
  this._updateElementContent(rateFeesElement, exchangeRateDealPresenter.rateFees);
  this._updateElementContent(dealElement, exchangeRateDealPresenter.deal);
  this._updateElementContent(remarkElement, exchangeRateDealPresenter.remark);
};

UpdateExchangeRateDealsView.prototype._updateElementContent = function(element, value) {
  element.innerHTML = value;
};

UpdateExchangeRateDealsView.prototype._updateBankRowElementToHide = function(bankRowElement) {
  bankRowElement.classList.add('no-rate');

  var rateFeesElement            = bankRowElement.querySelector(this.RATE_FEES_SELECTOR),
      dealElement                = bankRowElement.querySelector(this.DEAL_SELECTOR),
      remarkElement              = bankRowElement.querySelector(this.REMARK_SELECTOR);

  bankRowElement.setAttribute('data-deal', "99999999999");
  this._updateElementContent(rateFeesElement, '');
  this._updateElementContent(dealElement, '');
  this._updateElementContent(remarkElement, '');
};

UpdateExchangeRateDealsView.prototype._sortByDeal = function( itemElem ) {
  var dealStr = itemElem.getAttribute('data-deal'),
      deal = parseFloat(dealStr);

  if(deal > 0) {
    return deal;
  } else {
    return 0;
  }
};
