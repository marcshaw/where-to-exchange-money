function ParseCurrencies() {}

ParseCurrencies.prototype.execute = function (json) {
  return this._parseCurrencies(json['currencies']);
};

ParseCurrencies.prototype._parseCurrencies = function (json) {
  return json.map(function(currencyJson) {
    var id = currencyJson['id'];
    var name = currencyJson['name'];
    var code = currencyJson['code'];
    var symbol = currencyJson['symbol'];

    return new Currency(id, name, code, symbol);
  });
};
