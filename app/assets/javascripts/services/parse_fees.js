function ParseFees() {}

ParseFees.prototype.execute = function (json) {
  return this._parseFees(json['fees']);
};

ParseFees.prototype._parseFees = function (json) {
  return json.map(function(feeJson) {
    var id = feeJson['id'];
    var bankId = feeJson['bank_id'];
    var percentage = parseFloat(feeJson['percentage']);
    var minimum = parseFloat(feeJson['minimum']);
    var fromCurrencyId = parseFloat(feeJson['from_currency_id']);

    return new Fee(id, bankId, percentage, minimum, fromCurrencyId);
  });
};
