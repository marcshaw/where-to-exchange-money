module Presenters
  class ExchangeRates
    attr_reader :banks

    DEFAULT_FROM_CURRENCY_CODE = "NZD"
    DEFAULT_TO_CURRENCY_CODE = "EUR"

    def initialize(from_currencies, to_currencies, banks)
      @from_currencies = from_currencies
      @to_currencies = to_currencies
      @banks = banks
    end

    def default_from_currency
      Currency.find_by(code: DEFAULT_FROM_CURRENCY_CODE)
    end

    def default_to_currency
      Currency.find_by(code: DEFAULT_TO_CURRENCY_CODE)
    end

    def from_currencies
      @from_currencies.sort_by!(&:code)
    end

    def to_currencies
      (@to_currencies - [default_from_currency]).sort_by!(&:code)
    end
  end
end
