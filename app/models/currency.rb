class Currency < ApplicationRecord
  has_many :from_exchange_rates, class_name: "ExchangeRate", foreign_key: "from_id"
  has_many :to_exchange_rates,   class_name: "ExchangeRate", foreign_key: "to_id"

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :symbol, presence: true
end
