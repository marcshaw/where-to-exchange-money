module Commands
  class CreateExchangeRate
    attr_reader :rate, :to_currency, :from_currency, :bank, :recorded_at

    def initialize(rate:, to_currency:, from_currency:, bank:, recorded_at:)
      @rate = rate
      @to_currency = to_currency
      @from_currency = from_currency
      @bank = bank
      @recorded_at = recorded_at
    end

    def execute
      ExchangeRate.create(
        rate: rate,
        to_currency: to_currency,
        from_currency: from_currency,
        bank: bank,
        recorded_at: recorded_at,
      )
    end
  end
end
