module Queries
  class FindRecentFees
    def execute
      Fee.group("bank_id, from_currency_id").having('recorded_at = MAX(recorded_at)').includes(:bank, :from_currency)
    end
  end
end
