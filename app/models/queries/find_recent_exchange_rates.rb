# frozen_string_literal: true

module Queries
  class FindRecentExchangeRates
    def execute
      ExchangeRate.select("e.*").from("exchange_rates e").where("e.recorded_at > ?", Date.yesterday.to_datetime).joins("LEFT JOIN exchange_rates as x ON (x.bank_id = e.bank_id AND x.from_id = e.from_id AND x.to_id = e.to_id AND e.recorded_at < x.recorded_at)").where("x.recorded_at is NULL")
    end
  end
end
