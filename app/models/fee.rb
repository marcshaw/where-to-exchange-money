class Fee < ApplicationRecord
  belongs_to :bank
  belongs_to :from_currency, class_name: 'Currency', foreign_key: :from_currency_id

  validates :percentage, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
  validates :minimum, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :recorded_at, presence: true
end
