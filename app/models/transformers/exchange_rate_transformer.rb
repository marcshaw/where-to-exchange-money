module Transformers
  class ExchangeRateTransformer
    def transform(exchange_rate)
      {
        id: exchange_rate.id,
        bank_id: exchange_rate.bank_id,
        from_id: exchange_rate.from_id,
        to_id: exchange_rate.to_id,
        rate: exchange_rate.rate
      }
    end
  end
end
