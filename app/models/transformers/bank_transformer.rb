module Transformers
  class BankTransformer
    def transform(bank)
      { id: bank.id, name: bank.name }
    end
  end
end
