module Transformers
  class FeeTransformer
    def transform(fee)
      { id: fee.id, bank_id: fee.bank.id, from_currency_id: fee.from_currency.id, percentage: fee.percentage, minimum: fee.minimum }
    end
  end
end
