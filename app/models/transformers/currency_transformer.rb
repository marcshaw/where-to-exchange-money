module Transformers
  class CurrencyTransformer
    def transform(currency)
      { id: currency.id, name: currency.name, code: currency.code, symbol: currency.symbol }
    end
  end
end
