class ExchangeRate < ApplicationRecord
  belongs_to :bank
  belongs_to :from_currency, class_name: "Currency", foreign_key: "from_id"
  belongs_to :to_currency,   class_name: "Currency", foreign_key: "to_id"

  validates :rate, presence: true, numericality: { greater_than: 0 }
  validates :recorded_at, presence: true
end
