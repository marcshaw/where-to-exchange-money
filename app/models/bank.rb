class Bank < ApplicationRecord
  has_many :exchange_rates
  has_many :fees

  validates :name, presence: true, uniqueness: true
  validates :url, presence: true
end
