class ExchangeRatesController < ApplicationController
  def index
    # TODO factor into query object
    from_and_to_currency_id_pairs = ExchangeRate.pluck(:from_id, :to_id)
    from_currency_ids = from_and_to_currency_id_pairs.map(&:first)
    to_currency_ids = from_and_to_currency_id_pairs.map(&:second)
    currency_ids = (from_currency_ids + to_currency_ids).uniq
    currencies = Currency.where(id: currency_ids)

    from_currencies = currencies.select { |currency| from_currency_ids.include? currency.id }
    to_currencies = currencies.select { |currency| to_currency_ids.include? currency.id }
    banks = Bank.all

    @presenter = Presenters::ExchangeRates.new(from_currencies, to_currencies, banks)
  end
end
