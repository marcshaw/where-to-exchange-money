class Api::V1::ExchangeRatesController < ApplicationController
  def index
    payload = {
      exchange_rates:  transformed_exchange_rates,
      currencies: transformed_currencies,
      banks: transformed_banks,
      fees: transformed_fees
    }

    render json: payload
  end

  private

  def transformed_exchange_rates
    exchange_rates = Queries::FindRecentExchangeRates.new.execute
    exchange_rate_transformer = ::Transformers::ExchangeRateTransformer.new

    exchange_rates.collect { |exchange_rate| exchange_rate_transformer.transform(exchange_rate) }
  end

  def transformed_fees
    fees = Queries::FindRecentFees.new.execute
    fee_transformer = ::Transformers::FeeTransformer.new

    fees.collect { |fee| fee_transformer.transform(fee) }
  end

  def transformed_banks
    banks = Bank.all
    bank_transformer = ::Transformers::BankTransformer.new

    banks.collect { |bank| bank_transformer.transform(bank) }
  end

  def transformed_currencies
    currencies = Currency.all
    currency_transformer = ::Transformers::CurrencyTransformer.new

    currencies.collect { |currency| currency_transformer.transform(currency) }
  end
end
