FactoryGirl.define do
  factory :bank do
    name "Test Bank"
    url "#"

    factory :bank_with_fees do
      transient do
        fees_count 3
      end

      after(:create) do |bank, evaluator|
        create_list(:fee, evaluator.fees_count, bank: bank)
      end
    end

    factory :bank_with_exchange_rates do
      transient do
        exchange_rates_count 5
      end

      after(:create) do |bank, evaluator|
        create_list(:exchange_rate, evaluator.exchange_rates_count, bank: bank)
      end
    end

    factory :complete_bank do
      transient do
        fees_count 3
        exchange_rates_count 5
      end

      after(:create) do |bank, evaluator|
        create_list(:fee, evaluator.fees_count, bank: bank)
        create_list(:exchange_rate, evaluator.exchange_rates_count, bank: bank)
      end
    end
  end
end
