FactoryGirl.define do
  factory :currency do
    name   "Test Currency"
    code   "TC"
    symbol "$"
  end
end
