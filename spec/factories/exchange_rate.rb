FactoryGirl.define do
  factory :exchange_rate do
    association :bank, factory: :bank, name: "Bank With Exchange Rate"
    association :from_currency, factory: :currency, name: "Currency From", code: "Mike123"
    association :to_currency, factory: :currency, name: "Currency To", code: "Marc123"

    rate 1.2
    recorded_at DateTime.new(2017, 1, 1)
  end
end
