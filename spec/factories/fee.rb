FactoryGirl.define do
  factory :fee do
    association :bank, factory: :bank, name: "Bank With Fee"
    association :from_currency, factory: :currency

    recorded_at Time.now
    percentage 5
    minimum 10
  end
end
