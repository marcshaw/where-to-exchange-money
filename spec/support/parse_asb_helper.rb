module ParseAsbHelper
  def valid_raw_asb_data
    {"value"=>
     [{"currencyCode"=>"AED",
       "description"=>"UAE DIRHAM",
       "isFeatured"=>false,
       "smallestNote"=>"5",
       "buysNotes"=>3.1173,
       "buysPayments"=>2.7072,
       "sellsNotes"=>2.5063,
       "asbBuys"=>["Buy Notes"],
       "asbSells"=>["Sell Notes"]},
     {"currencyCode"=>"AUD",
      "description"=>"AUSTRALIAN DOLLAR",
      "isFeatured"=>true,
      "smallestNote"=>"5",
      "buysNotes"=>0.9555,
      "buysCheques"=>0.9349,
      "buysPayments"=>0.9301,
      "sellsNotes"=>0.8969,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"CAD",
      "description"=>"CANADIAN DOLLAR",
      "isFeatured"=>false,
      "smallestNote"=>"5",
      "buysNotes"=>0.9622,
      "buysCheques"=>0.9256,
      "buysPayments"=>0.9215,
      "sellsNotes"=>0.8854,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"CHF",
      "description"=>"SWISS FRANC",
      "isFeatured"=>false,
      "smallestNote"=>"10",
      "buysNotes"=>0.7409,
      "buysCheques"=>0.7126,
      "buysPayments"=>0.7109,
      "sellsNotes"=>0.6813,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"CNY",
      "description"=>"Yuan Renminbi",
      "isFeatured"=>false,
      "smallestNote"=>"1",
      "buysNotes"=>5.4121,
      "buysPayments"=>4.9091,
      "sellsNotes"=>4.4984,
      "asbBuys"=>["Buy Notes"],
      "asbSells"=>["Sell Notes"]},
     {"currencyCode"=>"DKK",
      "description"=>"DANISH KRONE",
      "isFeatured"=>false,
      "smallestNote"=>"50",
      "buysNotes"=>4.8716,
      "buysCheques"=>4.6825,
      "buysPayments"=>4.6483,
      "sellsNotes"=>4.4415,
      "asbBuys"=>["Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"EUR",
      "description"=>"EURO",
      "isFeatured"=>true,
      "smallestNote"=>"5",
      "buysNotes"=>0.6509,
      "buysCheques"=>0.631,
      "buysPayments"=>0.6276,
      "sellsNotes"=>0.5988,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"FJD",
      "description"=>"FIJI DOLLAR",
      "isFeatured"=>false,
      "smallestNote"=>"5",
      "buysNotes"=>1.6071,
      "buysPayments"=>1.5154,
      "sellsNotes"=>1.4141,
      "asbBuys"=>["Buy Notes"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"GBP",
      "description"=>"POUND STERLING",
      "isFeatured"=>true,
      "smallestNote"=>"5",
      "buysNotes"=>0.5971,
      "buysCheques"=>0.5788,
      "buysPayments"=>0.5746,
      "sellsNotes"=>0.5527,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"HKD",
      "description"=>"HONG KONG DOLLAR",
      "isFeatured"=>false,
      "smallestNote"=>"10",
      "buysNotes"=>6.0305,
      "buysCheques"=>5.7846,
      "buysPayments"=>5.7648,
      "sellsNotes"=>5.5389,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"INR",
      "description"=>"INDIAN RUPEE",
      "isFeatured"=>false,
      "buysPayments"=>47.2541,
      "sellsNotes"=>44.8961,
      "asbBuys"=>[],
      "asbSells"=>["Sell IMT"]},
     {"currencyCode"=>"JPY",
      "description"=>"YEN",
      "isFeatured"=>true,
      "smallestNote"=>"1000",
      "buysNotes"=>84.1473,
      "buysCheques"=>81.3371,
      "buysPayments"=>80.7434,
      "sellsNotes"=>77.4573,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"NOK",
      "description"=>"NORWEGIAN KRONE",
      "isFeatured"=>false,
      "smallestNote"=>"50",
      "buysNotes"=>6.0545,
      "buysCheques"=>5.8505,
      "buysPayments"=>5.7824,
      "sellsNotes"=>5.5298,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"PHP",
      "description"=>"PHILIPPINE PESO",
      "isFeatured"=>false,
      "buysPayments"=>37.7469,
      "sellsNotes"=>35.8007,
      "asbBuys"=>[],
      "asbSells"=>["Sell IMT"]},
     {"currencyCode"=>"SEK",
      "description"=>"SWEDISH KRONA",
      "isFeatured"=>false,
      "smallestNote"=>"20",
      "buysNotes"=>6.3099,
      "buysCheques"=>6.0213,
      "buysPayments"=>5.9484,
      "sellsNotes"=>5.6858,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"SGD",
      "description"=>"SINGAPORE DOLLAR",
      "isFeatured"=>false,
      "smallestNote"=>"2",
      "buysNotes"=>1.0489,
      "buysCheques"=>1.0081,
      "buysPayments"=>1.0027,
      "sellsNotes"=>0.9611,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"THB",
      "description"=>"BAHT",
      "isFeatured"=>false,
      "smallestNote"=>"20",
      "buysNotes"=>26.1208,
      "buysPayments"=>24.6013,
      "sellsNotes"=>23.4267,
      "asbBuys"=>["Buy Notes"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"TOP",
      "description"=>"PA'ANGA",
      "isFeatured"=>false,
      "smallestNote"=>"1",
      "buysNotes"=>1.7849,
      "buysPayments"=>1.6581,
      "sellsNotes"=>1.4543,
      "asbBuys"=>["Buy Notes"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"USD",
      "description"=>"US DOLLAR",
      "isFeatured"=>true,
      "smallestNote"=>"1",
      "buysNotes"=>0.7648,
      "buysCheques"=>0.7421,
      "buysPayments"=>0.7367,
      "sellsNotes"=>0.7083,
      "asbBuys"=>["Buy FX Cheque", "Buy Notes", "Buy Travellers Cheques"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"WST",
      "description"=>"TALA",
      "isFeatured"=>false,
      "smallestNote"=>"5",
      "buysNotes"=>2.0257,
      "buysPayments"=>1.9275,
      "sellsNotes"=>1.6852,
      "asbBuys"=>["Buy Notes"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]},
     {"currencyCode"=>"XPF",
      "description"=>"CPF FRANC",
      "isFeatured"=>false,
      "smallestNote"=>"500",
      "buysNotes"=>78.9316,
      "buysPayments"=>75.3841,
      "sellsNotes"=>70.7417,
      "asbBuys"=>["Buy Notes"],
      "asbSells"=>["Sell FX Cheque", "Sell Notes"]},
     {"currencyCode"=>"ZAR",
      "description"=>"RAND",
      "isFeatured"=>false,
      "smallestNote"=>"10",
      "buysNotes"=>10.1115,
      "buysPayments"=>9.7207,
      "sellsNotes"=>9.2961,
      "asbBuys"=>["Buy Notes"],
      "asbSells"=>["Sell FX Cheque", "Sell IMT", "Sell Notes"]}]}
  end

  def expected_parsed_asb_data
    [{:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"AED",
      :rate=>2.5063,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"AED",
      :to_currency=>"NZD",
      :rate=>3.1173,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"AUD",
      :rate=>0.8969,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"AUD",
      :to_currency=>"NZD",
      :rate=>0.9555,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"CAD",
      :rate=>0.8854,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"CAD",
      :to_currency=>"NZD",
      :rate=>0.9622,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"CHF",
      :rate=>0.6813,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"CHF",
      :to_currency=>"NZD",
      :rate=>0.7409,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"CNY",
      :rate=>4.4984,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"CNY",
      :to_currency=>"NZD",
      :rate=>5.4121,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"DKK",
      :rate=>4.4415,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"DKK",
      :to_currency=>"NZD",
      :rate=>4.8716,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"EUR",
      :rate=>0.5988,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"EUR",
      :to_currency=>"NZD",
      :rate=>0.6509,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"FJD",
      :rate=>1.4141,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"FJD",
      :to_currency=>"NZD",
      :rate=>1.6071,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"GBP",
      :rate=>0.5527,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"GBP",
      :to_currency=>"NZD",
      :rate=>0.5971,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"HKD",
      :rate=>5.5389,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"HKD",
      :to_currency=>"NZD",
      :rate=>6.0305,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"INR",
      :rate=>44.8961,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"INR",
      :to_currency=>"NZD",
      :rate=>nil,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"JPY",
      :rate=>77.4573,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"JPY",
      :to_currency=>"NZD",
      :rate=>84.1473,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"NOK",
      :rate=>5.5298,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NOK",
      :to_currency=>"NZD",
      :rate=>6.0545,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"PHP",
      :rate=>35.8007,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"PHP",
      :to_currency=>"NZD",
      :rate=>nil,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"SEK",
      :rate=>5.6858,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"SEK",
      :to_currency=>"NZD",
      :rate=>6.3099,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"SGD",
      :rate=>0.9611,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"SGD",
      :to_currency=>"NZD",
      :rate=>1.0489,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"THB",
      :rate=>23.4267,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"THB",
      :to_currency=>"NZD",
      :rate=>26.1208,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"TOP",
      :rate=>1.4543,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"TOP",
      :to_currency=>"NZD",
      :rate=>1.7849,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"USD",
      :rate=>0.7083,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"USD",
      :to_currency=>"NZD",
      :rate=>0.7648,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"WST",
      :rate=>1.6852,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"WST",
      :to_currency=>"NZD",
      :rate=>2.0257,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"XPF",
      :rate=>70.7417,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"XPF",
      :to_currency=>"NZD",
      :rate=>78.9316,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"NZD",
      :to_currency=>"ZAR",
      :rate=>9.2961,
      :recorded_at=>Time.now.change(:sec => 0)},
     {:bank=>"asb",
      :from_currency=>"ZAR",
      :to_currency=>"NZD",
      :rate=>10.1115,
      :recorded_at=>Time.now.change(:sec => 0)}]
  end
end
