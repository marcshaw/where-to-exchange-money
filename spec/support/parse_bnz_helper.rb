module ParseBnzHelper
  def valid_raw_bnz_data
    {"exchange_rates"=>[{"currency"=>"AUD", "notebuy"=>"0.9502", "notesell"=>"0.8975"}, {"currency"=>"BHD", "notebuy"=>"", "notesell"=>""}, {"currency"=>"CAD", "notebuy"=>"0.9531", "notesell"=>"0.8863"}, {"currency"=>"CNY", "notebuy"=>"5.4078", "notesell"=>"4.4873"}]}
  end

  def expected_parsed_bnz_data
    [{:bank=>"bnz", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.8975", :recorded_at=>Time.now.change(:sec => 0)}, {:bank=>"bnz", :from_currency=>"AUD", :to_currency=>"NZD", :rate=>"0.9502",:recorded_at=>Time.now.change(:sec => 0) }, {:bank=>"bnz", :from_currency=>"NZD", :to_currency=>"BHD", :rate=>"", :recorded_at=>Time.now.change(:sec => 0)}, {:bank=>"bnz", :from_currency=>"BHD", :to_currency=>"NZD", :rate=>"", :recorded_at=>Time.now.change(:sec => 0)}, {:bank=>"bnz", :from_currency=>"NZD", :to_currency=>"CAD", :rate=>"0.8863", :recorded_at=>Time.now.change(:sec => 0)}, {:bank=>"bnz", :from_currency=>"CAD", :to_currency=>"NZD", :rate=>"0.9531", :recorded_at=>Time.now.change(:sec => 0)}, {:bank=>"bnz", :from_currency=>"NZD", :to_currency=>"CNY", :rate=>"4.4873", :recorded_at=>Time.now.change(:sec => 0)}, {:bank=>"bnz", :from_currency=>"CNY", :to_currency=>"NZD", :rate=>"5.4078", :recorded_at=>Time.now.change(:sec => 0)}]
  end
end
