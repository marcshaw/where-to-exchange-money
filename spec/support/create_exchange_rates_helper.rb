module CreateExchangeRatesHelper
  def valid_mapped_data
    [
      { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9188", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"0.6849", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"asb", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9238", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"asb", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"0.6916", :recorded_at=>Time.now.change(:sec => 0) },
    ]
  end

  def partially_valid_mapped_data
    [
      { :bank=>"", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9188", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"0.6849", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"asb", :from_currency=>"", :to_currency=>"AUD", :rate=>"0.9238", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"asb", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"0.6916", :recorded_at=>Time.now.change(:sec => 0) },
    ]
  end

  def create_records(data)
    data.each do |data|
      FactoryGirl.create(:bank, name: data[:bank]) unless existing_bank?(data[:bank])
      FactoryGirl.create(:currency, name: data[:to_currency], code: data[:to_currency]) unless existing_currency?(data[:to_currency])
      FactoryGirl.create(:currency, name: data[:from_currency], code: data[:from_currency]) unless existing_currency?(data[:from_currency])
    end
  end

  private

  def existing_bank?(name)
    name.blank? || Bank.find_by(name: name).present?
  end

  def existing_currency?(code)
    code.blank? || Currency.find_by(code: code).present?
  end
end
