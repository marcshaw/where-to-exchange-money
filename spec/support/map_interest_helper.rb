module MapInterestHelper
  def valid_parsed_interest_data
    {
      "anz" => {:position=>3, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9188"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>"0.6849"}}},
      "asb" => {:position=>4, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9238"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>"0.6916"}}},
      "bnz" => {:position=>5, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9244"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>"0.6923"}}},
      "westpac" => {:position=>6, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9171"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>""}}}
    }
  end

  def expected_mapped_interest_data
    [
      { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9188", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"0.6849", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"asb", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9238", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"asb", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"0.6916", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"bnz", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9244", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"bnz", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"0.6923", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"westpac", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9171", :recorded_at=>Time.now.change(:sec => 0) },
      { :bank=>"westpac", :from_currency=>"NZD", :to_currency=>"CHF", :rate=>"", :recorded_at=>Time.now.change(:sec => 0) }
    ]
  end
end
