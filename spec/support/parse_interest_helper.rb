module ParseInterestHelper
  def valid_raw_interest_data
    {"exchange_rates"=>[{"data"=>[{"value"=>"Code"}, {"value"=>"Currency"}, {"value"=>" "}, {"value"=>"ANZ"}, {"value"=>"ASB"}, {"value"=>"BNZ"}, {"value"=>"Westpac"}]}, {"data"=>[{"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}]}, {"data"=>[{"value"=>"AUD"}, {"value"=>"Australian dollar"}, {"value"=>" "}, {"value"=>"0.9188"}, {"value"=>"0.9238"}, {"value"=>"0.9244"}, {"value"=>"0.9171"}]}, {"data"=>[{"value"=>"CHF"}, {"value"=>"Swiss franc"}, {"value"=>" "}, {"value"=>"0.6849"}, {"value"=>"0.6916"}, {"value"=>"0.6923"}, {"value"=>""}]}, {"data"=>[{"value"=>"EUR"}, {"value"=>"Euro"}, {"value"=>" "}, {"value"=>"0.6205"}, {"value"=>"0.6253"}, {"value"=>"0.6258"}, {"value"=>"0.6202"}]}, {"data"=>[{"value"=>"FJD"}, {"value"=>"Fiji dollar"}, {"value"=>" "}, {"value"=>"1.4454"}, {"value"=>"1.4355"}, {"value"=>"1.4234"}, {"value"=>""}]}, {"data"=>[{"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}, {"value"=>" "}]}, {"data"=>[{"value"=>" "}, {"value"=>"Fee"}, {"value"=>" "}, {"value"=>"1.1%"}, {"value"=>"1.0%"}, {"value"=>"1.0%"}, {"value"=>"1.0%"}]}, {"data"=>[{"value"=>" "}, {"value"=>"Fee minimum"}, {"value"=>" "}, {"value"=>"$12"}, {"value"=>"$10"}, {"value"=>"$10"}, {"value"=>"$10"}]}]}
  end

  def invalid_raw_interest_data
    {"not_real_data" => [{"data" => ["data is faked"]}]}
  end

  def expected_parsed_interest_data
    { "anz" => {:position=>3, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9188"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>"0.6849"}, :EUR=>{:currency_name=>"Euro", :rate=>"0.6205"}, :FJD=>{:currency_name=>"Fiji dollar", :rate=>"1.4454"}}},
      "asb" => {:position=>4, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9238"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>"0.6916"}, :EUR=>{:currency_name=>"Euro", :rate=>"0.6253"}, :FJD=>{:currency_name=>"Fiji dollar", :rate=>"1.4355"}}},
      "bnz" => {:position=>5, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9244"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>"0.6923"}, :EUR=>{:currency_name=>"Euro", :rate=>"0.6258"}, :FJD=>{:currency_name=>"Fiji dollar", :rate=>"1.4234"}}},
      "westpac" => {:position=>6, :currencies=>{:AUD=>{:currency_name=>"Australian dollar", :rate=>"0.9171"}, :CHF=>{:currency_name=>"Swiss franc", :rate=>""}, :EUR=>{:currency_name=>"Euro", :rate=>"0.6202"}, :FJD=>{:currency_name=>"Fiji dollar", :rate=>""}}}}
  end
end
