require 'rails_helper'

RSpec.describe ExchangeRate, type: :model do
  let(:bank) { create(:bank) }
  let(:currency_a) { create(:currency, name: "Currency A", code: "CA") }
  let(:currency_b) { create(:currency, name: "Currency B", code: "CB") }
  let(:rate) { 1.2 }
  let(:recorded_at) { DateTime.new(2017, 1, 1) }

  describe "#valid?" do
    let(:exchange_rate) { ExchangeRate.new(options) }

    context "when the exchange rate has currencies, a rate, and a recorded_at" do
      let(:context_options) { { from_currency: currency_a, to_currency: currency_b, rate: rate, recorded_at: recorded_at } }

      context "when the exchange rate has a bank" do
        let(:options) { context_options.merge(bank: bank) }

        it "is valid" do
          expect(exchange_rate).to be_valid
        end
      end

      context "when the exchange rate has no bank" do
        let(:options) { context_options }

        it "is invalid" do
          expect(exchange_rate).not_to be_valid
        end
      end
    end

    context "when the exchange rate has a bank, a to currency, a rate, and a recorded at" do
      let(:context_options) { { bank: bank, to_currency: currency_b, rate: rate, recorded_at: recorded_at } }

      context "when the exchange rate has a from currency" do
        let(:options) { context_options.merge(from_currency: currency_a) }

        it "is valid" do
          expect(exchange_rate).to be_valid
        end
      end

      context "when the exchange rate has no from currency" do
        let(:options) { context_options }

        it "is invalid" do
          expect(exchange_rate).not_to be_valid
        end
      end
    end

    context "when the exchange rate has a bank, a from currency, a rate, and a recorded at" do
      let(:context_options) { { bank: bank, from_currency: currency_a, rate: rate, recorded_at: recorded_at } }

      context "when the exchange rate has a to currency" do
        let(:options) { context_options.merge(to_currency: currency_b) }

        it "is valid" do
          expect(exchange_rate).to be_valid
        end
      end

      context "when the exchange rate has no to currency" do
        let(:options) { context_options }

        it "is invalid" do
          expect(exchange_rate).not_to be_valid
        end
      end
    end

    context "when the exchange rate has a bank, a from currency, a to currency, and a recorded at" do
      let(:context_options) { { bank: bank, from_currency: currency_a, to_currency: currency_b, recorded_at: recorded_at } }

      context "when the exchange rate has a rate" do
        let(:options) { context_options.merge(rate: rate) }
        let(:rate) { 1.2 }

        it "is valid" do
          expect(exchange_rate).to be_valid
        end

        context "when the rate is 0" do
          let(:rate) { 0 }

          it "is invalid" do
            expect(exchange_rate).not_to be_valid
          end
        end

        context "when the rate is negative" do
          let(:rate) { -1 }

          it "is invalid" do
            expect(exchange_rate).not_to be_valid
          end
        end
      end

      context "when the exchange rate has a rate that is not a number" do
        let(:options) { context_options.merge(rate: rate) }
        let(:rate) { "test" }

        it "is invalid" do
          expect(exchange_rate).not_to be_valid
        end
      end

      context "when the exchange rate has no rate" do
        let(:options) { context_options }

        it "is invalid" do
          expect(exchange_rate).not_to be_valid
        end
      end
    end

    context "when the exchange rate has a bank, a from currency, a to currency, and a rate" do
      let(:context_options) { { bank: bank, from_currency: currency_a, to_currency: currency_b, rate: rate } }

      context "when the exchange rate has a recorded at" do
        let(:options) { context_options.merge(recorded_at: recorded_at) }

        it "is valid" do
          expect(exchange_rate).to be_valid
        end
      end

      context "when the exchange rate has no recorded at" do
        let(:options) { context_options }

        it "is invalid" do
          expect(exchange_rate).not_to be_valid
        end
      end
    end
  end
end
