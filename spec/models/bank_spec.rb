require 'rails_helper'

RSpec.describe Bank, type: :model do
  describe "#valid?" do
    let(:bank) { Bank.new(options) }

    context "when the bank has a name and url" do
      let(:options) { { name: "Test bank", url: "#" } }

      it "is valid" do
        expect(bank).to be_valid
      end
    end

    context "when the bank has a name" do
      let(:options) { { name: "Test Bank" } }

      it "is valid" do
        expect(bank).not_to be_valid
      end
    end

    context "when the bank has a blank name and a url" do
      let(:options) { { name: "", url: "#" } }

      it "is invalid" do
        expect(bank).not_to be_valid
      end
    end

    context "when the bank has no name and a url" do
      let(:options) { { url: "#" } }

      it "is invalid" do
        expect(bank).not_to be_valid
      end
    end

    context "when the bank has a url" do
      let(:options) { { url: "#" } }

      it "is valid" do
        expect(bank).not_to be_valid
      end
    end

    context "when the bank has a blank url and a name" do
      let(:options) { { name: "Test bank", url: "" } }

      it "is invalid" do
        expect(bank).not_to be_valid
      end
    end

    context "when the bank has no url and a name" do
      let(:options) { { name: "Test bank" } }

      it "is invalid" do
        expect(bank).not_to be_valid
      end
    end

    context "when the bank has no name or url" do
      let(:options) { {} }

      it "is invalid" do
        expect(bank).not_to be_valid
      end
    end
  end
end
