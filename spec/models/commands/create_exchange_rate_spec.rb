require "rails_helper"

RSpec.describe Commands::CreateExchangeRate do

  subject(:create_exchange_rate) { Commands::CreateExchangeRate.new(params) }

  let(:exchange_rate) { build(:exchange_rate) }

  let(:params) do
    {
      bank: exchange_rate.bank,
      from_currency: exchange_rate.from_currency,
      to_currency: exchange_rate.to_currency,
      rate: exchange_rate.rate,
      recorded_at: exchange_rate.recorded_at,
    }
  end

  context "when given valid data" do
    it "creates a new exchange rate" do
      expect { create_exchange_rate.execute }.to change { ExchangeRate.count }.by(1)
    end
  end

  context "when given invalid data" do
    before { params[:rate] = -1 }

    it "does not create any exchange rates" do
      expect { create_exchange_rate.execute }.to change { ExchangeRate.count }.by(0)
    end
  end
end
