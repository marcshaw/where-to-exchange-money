require 'rails_helper'

RSpec.describe Fee, type: :model do
  let(:bank) { create(:bank) }
  let(:percentage) { 5 }
  let(:minimum) { 10 }
  let(:recorded_at) { DateTime.new(2017, 1, 1) }
  let(:currency) { create(:currency) }

  describe "#valid?" do
    let(:fee) { Fee.new(options) }

    context "when the fee has a percentage, a minimum, a currency, and a recorded at" do
      let(:context_options) { { percentage: percentage, minimum: minimum, recorded_at: recorded_at, from_currency: currency } }

      context "when the fee has a bank" do
        let(:options) { context_options.merge(bank: bank) }

        it "is valid" do
          expect(fee).to be_valid
        end
      end

      context "when the fee has no bank" do
        let(:options) { context_options }

        it "is invalid" do
          expect(fee).not_to be_valid
        end
      end
    end

    context "when the fee has a bank, a minimum, a currency, and a recorded at" do
      let(:context_options) { { bank: bank, minimum: minimum, recorded_at: recorded_at, from_currency: currency } }

      context "when the fee has a percentage" do
        let(:options) { context_options.merge(percentage: percentage) }
        let(:percentage) { 5 }

        it "is valid" do
          expect(fee).to be_valid
        end

        context "when the percentage is less than 0" do
          let(:percentage) { -1 }

          it "is invalid" do
            expect(fee).not_to be_valid
          end
        end

        context "when the percentage is over 100" do
          let(:percentage) { 101 }

          it "is invalid" do
            expect(fee).not_to be_valid
          end
        end

        context "when the fee has a percentage that is not a number" do
          let(:percentage) { "test" }
          let(:options) { context_options.merge(percentage: percentage) }

          it "is invalid" do
            expect(fee).not_to be_valid
          end
        end
      end

      context "when the fee has no percentage" do
        let(:options) { context_options }

        it "is invalid" do
          expect(fee).not_to be_valid
        end
      end
    end

    context "when the fee has a bank, a percentage, a currency, and a recorded at" do
      let(:context_options) { { bank: bank, percentage: percentage, recorded_at: recorded_at, from_currency: currency } }

      context "when the fee has a minimum" do
        let(:options) { context_options.merge(minimum: minimum) }
        let(:minimum) { 10 }

        it "is valid" do
          expect(fee).to be_valid
        end

        context "when the minimum is 0" do
          let(:minimum) { 0 }

          it "is valid" do
            expect(fee).to be_valid
          end
        end

        context "when the minimum is negative" do
          let(:minimum) { -1 }

          it "is invalid" do
            expect(fee).not_to be_valid
          end
        end
      end

      context "when the fee has a minimum that is not a number" do
        let(:options) { context_options.merge(minimum: minimum) }
        let(:minimum) { "test" }

        it "is invalid" do
          expect(fee).not_to be_valid
        end
      end

      context "when the fee has no minimum" do
        let(:options) { context_options }

        it "is invalid" do
          expect(fee).not_to be_valid
        end
      end
    end

    context "when the fee has a bank, a percentage, a currency, and a minimum" do
      let(:context_options) { { bank: bank, percentage: percentage, minimum: minimum, from_currency: currency } }

      context "when the fee has a recorded at" do
        let(:options) { context_options.merge(recorded_at: recorded_at) }

        it "is valid" do
          expect(fee).to be_valid
        end
      end

      context "when the fee has no recorded at" do
        let(:options) { context_options }

        it "is invalid" do
          expect(fee).not_to be_valid
        end
      end
    end

    context "when the fee has a bank, a percentage, a recorded at, and a minimum" do
      let(:context_options) { { bank: bank, percentage: percentage, minimum: minimum, recorded_at: Time.now } }

      context "when the fee has a currency" do
        let(:options) { context_options.merge(from_currency: currency) }

        it "is valid" do
          expect(fee).to be_valid
        end
      end

      context "when the fee has no currency" do
        let(:options) { context_options }

        it "is invalid" do
          expect(fee).not_to be_valid
        end
      end
    end
  end
end
