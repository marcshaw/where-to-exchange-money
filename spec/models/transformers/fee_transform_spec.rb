require 'rails_helper'
RSpec.describe Transformers::FeeTransformer do
  subject(:transformed_fee) { Transformers::FeeTransformer.new.transform(fee) }
  let(:fee) { create(:fee) }

  let(:ignored_attributes) { ["created_at", "updated_at", "recorded_at"] }
  let(:fee_attributes) { Fee.attribute_names - ignored_attributes }

  it "returns a hash with all the fee's attirbutes" do
    expect(transformed_fee.stringify_keys.keys).to match_array(fee_attributes)
  end
end
