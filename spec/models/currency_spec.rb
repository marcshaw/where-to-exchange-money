require 'rails_helper'

RSpec.describe Currency, type: :model do
  describe "#valid?" do
    let(:currency) { Currency.new(options) }

    context "when the currency has a name, code, and symbol" do
      let(:options) { { name: name, code: code, symbol: "$" } }
      let(:existing_name) { "Existing Name" }
      let(:existing_code) { "EN" }

      before do
        Currency.create!(name: existing_name, code: existing_code, symbol: "$")
      end

      context "when the name is unique" do
        let(:name) { "New Name" }
        let(:code) { "NC" }

        it "is valid" do
          expect(currency).to be_valid
        end
      end

      context "when the name is not unique" do
        let(:name) { existing_name }
        let(:code) { "NC" }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end

      context "when the code is unique" do
        let(:name) { "New Name" }
        let(:code) { "NC" }

        it "is valid" do
          expect(currency).to be_valid
        end
      end

      context "when the code is not unique" do
        let(:name) { "New Name" }
        let(:code) { existing_code }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end
    end

    context "when the currency has a code and symbol" do
      let(:context_options) { { code: "TEST", symbol: "$" } }

      context "when the currency has a name" do
        let(:options) { context_options.merge(name: "Test Currency") }

        it "is valid" do
          expect(currency).to be_valid
        end
      end

      context "when the currency has a blank name" do
        let(:options) { context_options.merge(name: "") }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end

      context "when the currency has no name" do
        let(:options) { context_options }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end
    end

    context "when the currency has a name and symbol" do
      let(:context_options) { { name: "Test Currency", symbol: "$" } }

      context "when the currency has a code" do
        let(:options) { context_options.merge(code: "TEST") }

        it "is valid" do
          expect(currency).to be_valid
        end
      end

      context "when the currency has a blank code" do
        let(:options) { context_options.merge(code: "") }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end

      context "when the currency has no code" do
        let(:options) { context_options }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end
    end

    context "when the currency has a name and code" do
      let(:context_options) { { name: "Test Currency", code: "TEST" } }

      context "when the currency has a symbol" do
        let(:options) { context_options.merge(symbol: "$") }

        it "is valid" do
          expect(currency).to be_valid
        end
      end

      context "when the currency has a blank symbol" do
        let(:options) { context_options.merge(symbol: "") }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end

      context "when the currency has no symbol" do
        let(:options) { context_options }

        it "is invalid" do
          expect(currency).not_to be_valid
        end
      end
    end
  end
end
