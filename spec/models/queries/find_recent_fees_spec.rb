require 'rails_helper'

RSpec.describe Queries::FindRecentFees do
  let(:find_recent_fees) { Queries::FindRecentFees.new }

  describe "#execute" do
    let(:results) { find_recent_fees.execute }

    context "when fees exist" do
      let(:kiwibank) { create(:bank, name: "Kiwibank") }
      let(:westpac) { create(:bank, name: "Westpac") }

      let(:nzd) { create(:currency, code: "NZD", name: "Nzd") }
      let(:aud) { create(:currency, code: "AUD", name: "Aud") }

      let!(:kiwibank_nzd_fee_1) { create(:fee, percentage: 10, bank: kiwibank, recorded_at: Time.new(2017, 1, 1), from_currency: nzd) }
      let!(:kiwibank_nzd_fee_2) { create(:fee, percentage: 20, bank: kiwibank, recorded_at: Time.new(2017, 1, 2), from_currency: nzd) }
      let!(:kiwibank_aud_fee_1) { create(:fee, percentage: 20, bank: kiwibank, recorded_at: Time.new(2017, 1, 2), from_currency: aud) }
      let!(:westpac_nzd_fee_1)  { create(:fee, percentage: 15, bank: westpac, recorded_at: Time.new(2017, 1, 1), from_currency: nzd) }
      let!(:westpac_aud_fee_1)  { create(:fee, percentage: 15, bank: westpac, recorded_at: Time.new(2017, 1, 1), from_currency: aud) }

      it "finds the most recent fees" do
        expect(results).to contain_exactly(kiwibank_nzd_fee_2, kiwibank_aud_fee_1, westpac_nzd_fee_1, westpac_aud_fee_1)
      end
    end

    context "when no fees exist" do
      it "finds nothing" do
        expect(results).to be_empty
      end
    end
  end
end
