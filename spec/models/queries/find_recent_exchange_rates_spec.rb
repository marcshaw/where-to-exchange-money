require 'rails_helper'

RSpec.describe Queries::FindRecentExchangeRates do
  let(:find_recent_exchange_rates) { Queries::FindRecentExchangeRates.new }

  describe "#execute" do
    subject(:results) { find_recent_exchange_rates.execute }

    context "when exchange rates exist" do
      let(:kiwibank) { create(:bank, name: "Kiwibank") }
      let(:westpac) { create(:bank, name: "Westpac") }
      let(:anz) { create(:bank, name: "ANZ") }

      let(:nzd) { create(:currency, name: "New Zealand Dollar", code: "NZD") }
      let(:aud) { create(:currency, name: "Australian Dollar", code: "AUD") }
      let(:usd) { create(:currency, name: "United States Dollar", code: "USD") }

      let(:today)     { Date.today.to_datetime }
      let(:yesterday) { Date.yesterday.to_datetime + 1.minute }
      let(:last_week) { 7.days.ago }

      let!(:nzd_to_aud_1) { create(:exchange_rate, bank: kiwibank, from_currency: nzd, to_currency: aud, recorded_at: yesterday, rate: 1) }
      let!(:nzd_to_aud_2) { create(:exchange_rate, bank: kiwibank, from_currency: nzd, to_currency: aud, recorded_at: today, rate: 2) }
      let!(:nzd_to_aud_3) { create(:exchange_rate, bank: westpac,  from_currency: nzd, to_currency: aud, recorded_at: yesterday, rate: 3) }
      let!(:nzd_to_aud_4) { create(:exchange_rate, bank: anz,      from_currency: nzd, to_currency: aud, recorded_at: last_week, rate: 4) }

      let!(:nzd_to_usd_1) { create(:exchange_rate, bank: kiwibank, from_currency: nzd, to_currency: usd, recorded_at: yesterday, rate: 5) }
      let!(:nzd_to_usd_2) { create(:exchange_rate, bank: westpac,  from_currency: nzd, to_currency: usd, recorded_at: today, rate: 6) }
      let!(:nzd_to_usd_3) { create(:exchange_rate, bank: westpac,  from_currency: nzd, to_currency: usd, recorded_at: yesterday, rate: 7) }
      let!(:nzd_to_usd_4) { create(:exchange_rate, bank: anz,      from_currency: nzd, to_currency: usd, recorded_at: today, rate: 8) }

      it "finds the most recent exchange rates" do
        expect(results).to contain_exactly(nzd_to_aud_2, nzd_to_aud_3, nzd_to_usd_1, nzd_to_usd_2, nzd_to_usd_4)
      end
    end

    context "when no exchange rates exist" do
      it "finds nothing" do
        expect(results).to be_empty
      end
    end
  end
end
