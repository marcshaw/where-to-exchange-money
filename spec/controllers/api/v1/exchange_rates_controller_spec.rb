require 'rails_helper'
RSpec.describe Api::V1::ExchangeRatesController, :type => :controller do
  describe "#index" do
    it "returns json" do
      get :index

      expect(response.content_type).to eq("application/json")
    end

    context "without data" do
      it "returns an empty response" do
        get :index
        json_response = JSON.parse(response.body)

        expect(json_response["exchange_rates"]).to be_empty
        expect(json_response["fees"]).to be_empty
        expect(json_response["banks"]).to be_empty
        expect(json_response["currencies"]).to be_empty
      end
    end

    context "with data" do
      let(:kiwibank) { create(:bank, name: "Kiwibank") }
      let(:westpac) { create(:bank, name: "Westpac") }

      let(:nzd) { create(:currency, name: "New Zealand Dollar", code: "NZD") }
      let(:aud) { create(:currency, name: "Australian Dollar", code: "AUD") }
      let(:usd) { create(:currency, name: "United States Dollar", code: "USD") }

      let(:today) { Date.today.to_datetime }

      let(:kiwibank_fee) { create(:fee, percentage: 10, bank: kiwibank, recorded_at: today, from_currency: nzd) }
      let(:westpac_fee)  { create(:fee, percentage: 15, bank: westpac,  recorded_at: today, from_currency: nzd) }

      let(:nzd_to_aud_kiwibank) { create(:exchange_rate, bank: kiwibank, from_currency: nzd, to_currency: aud, recorded_at: today) }
      let(:nzd_to_aud_westpac)  { create(:exchange_rate, bank: westpac,  from_currency: nzd, to_currency: aud, recorded_at: today) }
      let(:nzd_to_usd_kiwibank) { create(:exchange_rate, bank: kiwibank, from_currency: nzd, to_currency: usd, recorded_at: today) }
      let(:nzd_to_usd_westpac)  { create(:exchange_rate, bank: westpac,  from_currency: nzd, to_currency: usd, recorded_at: today) }

      let(:usd_to_nzd_westpac)  { create(:exchange_rate, bank: westpac,  from_currency: usd, to_currency: nzd, recorded_at: today) }
      let(:aud_to_nzd_westpac)  { create(:exchange_rate, bank: westpac,  from_currency: aud, to_currency: nzd, recorded_at: today) }

      let!(:exchange_rate_ids) { [nzd_to_aud_kiwibank, nzd_to_aud_westpac, nzd_to_usd_kiwibank, nzd_to_usd_westpac, usd_to_nzd_westpac, aud_to_nzd_westpac].collect(&:id) }
      let!(:fee_ids) { [kiwibank_fee, westpac_fee].collect(&:id) }
      let!(:bank_ids) { [kiwibank, westpac].collect(&:id) }
      let!(:currency_ids) { [nzd, aud, usd].collect(&:id) }

      it "returns exchange rates data with fees" do
        get :index
        json_response = JSON.parse(response.body)

        expect(json_response["exchange_rates"].pluck("id")).to contain_exactly(*exchange_rate_ids)
        expect(json_response["fees"].pluck("id")).to contain_exactly(*fee_ids)
        expect(json_response["banks"].pluck("id")).to contain_exactly(*bank_ids)
        expect(json_response["currencies"].pluck("id")).to contain_exactly(*currency_ids)
      end
    end
  end
end
