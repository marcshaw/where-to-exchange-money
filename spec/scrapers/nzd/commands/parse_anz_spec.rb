require "rails_helper"
require 'support/parse_anz_helper'

RSpec.describe Nzd::Commands::ParseAnz do
  include ParseAnzHelper

  subject(:parse_anz) { Nzd::Commands::ParseAnz.new(data: data).execute }

  context "when given valid data" do
    let(:data) { valid_raw_anz_data }

    it "returns a hash with the approriate data" do
      expect(parse_anz).to eq(expected_parsed_anz_data)
    end
  end
end
