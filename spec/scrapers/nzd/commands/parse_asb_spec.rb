require "rails_helper"
require 'support/parse_asb_helper'

RSpec.describe Nzd::Commands::ParseAsb do
  include ParseAsbHelper

  subject(:parse_asb) { Nzd::Commands::ParseAsb.new(data: data).execute }

  context "when given valid data" do
    let(:data) { valid_raw_asb_data }

    it "returns a hash with the approriate data" do
      expect(parse_asb).to eq(expected_parsed_asb_data)
    end
  end
end
