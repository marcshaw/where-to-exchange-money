require "rails_helper"
require 'support/parse_bnz_helper'

RSpec.describe Nzd::Commands::ParseBnz do
  include ParseBnzHelper

  subject(:parse_bnz) { Nzd::Commands::ParseBnz.new(data: data).execute }

  context "when given valid data" do
    let(:data) { valid_raw_bnz_data }

    it "returns a hash with the approriate data" do
      expect(parse_bnz).to eq(expected_parsed_bnz_data)
    end
  end
end
