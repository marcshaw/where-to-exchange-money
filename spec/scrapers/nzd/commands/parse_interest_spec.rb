require "rails_helper"
require 'support/parse_interest_helper'

RSpec.describe Nzd::Commands::ParseInterest do
  include ParseInterestHelper

  subject(:parse_interest) { Nzd::Commands::ParseInterest.new(data: data).execute }

  context "when given valid data" do
    let(:data) { valid_raw_interest_data }

    it "returns a hash with the approriate data" do
      expect(parse_interest).to eq(expected_parsed_interest_data)
    end
  end

  context "when given invalid data" do
    let(:data) { invalid_raw_interest_data }

    it "returns nil" do
      expect { parse_interest } .to raise_error(ArgumentError)
    end
  end
end

