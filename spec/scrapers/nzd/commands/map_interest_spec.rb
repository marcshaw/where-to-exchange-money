require "rails_helper"
require 'support/map_interest_helper'

RSpec.describe Nzd::Commands::MapInterest do
  include MapInterestHelper

  subject(:map_interest) { Nzd::Commands::MapInterest.new(parsed_data: data).execute }

  context "when given valid data" do
    let(:data) { valid_parsed_interest_data }

    it "returns a hash with the approriate data" do
      expect(map_interest).to eq(expected_mapped_interest_data)
    end
  end

  context "when given invalid data" do
    context "when the data does match structure" do
      let(:data) { {"not_real_data" => {"data" => ["data is faked"]}} }

      it "returns throws an error" do
        expect { map_interest } .to raise_error(ArgumentError)
      end
    end

    context "when the data is not a hash" do
      let(:data) { "heh" }

      it "returns throws an error" do
        expect { map_interest } .to raise_error(ArgumentError)
      end
    end
  end
end

