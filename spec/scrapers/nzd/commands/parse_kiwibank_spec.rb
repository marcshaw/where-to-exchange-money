require "rails_helper"
require 'support/parse_kiwibank_helper'

RSpec.describe Nzd::Commands::ParseKiwibank do
  include ParseKiwibankHelper

  subject(:parse_kiwibank) { Nzd::Commands::ParseKiwibank.new(data: data).execute }

  context "when given valid data" do
    let(:data) { valid_raw_kiwibank_data }

    it "returns a hash with the approriate data" do
      expect(parse_kiwibank).to eq(expected_parsed_kiwibank_data)
    end
  end
end
