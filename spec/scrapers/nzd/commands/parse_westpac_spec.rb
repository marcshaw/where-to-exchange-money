require "rails_helper"
require 'support/parse_westpac_helper'

RSpec.describe Nzd::Commands::ParseWestpac do
  include ParseWestpacHelper

  subject(:parse_westpac) { Nzd::Commands::ParseWestpac.new(data: data).execute }

  context "when given valid data" do
    let(:data) { valid_raw_westpac_data }

    it "returns a hash with the approriate data" do
      expect(parse_westpac).to eq(expected_parsed_westpac_data)
    end
  end
end
