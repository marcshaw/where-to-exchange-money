require "rails_helper"
require 'support/create_exchange_rates_helper'

RSpec::Matchers.define :matching_mapped_hash do |mapped_hash|
  match do |actual|
    mapped_hash.any? do |hash|
      hash[:bank] == actual[:bank].name &&
        hash[:to_currency] == actual[:to_currency].code &&
        hash[:from_currency] == actual[:from_currency].code &&
        hash[:rate] == actual[:rate] &&
        hash[:recorded_at] == actual[:recorded_at]
    end
  end
end

RSpec.describe Commands::CreateExchangeRates do
  include CreateExchangeRatesHelper

  subject(:create_exchange_rates) { Commands::CreateExchangeRates.new(mapped_data: data, create_exchange_rate: create_exchange_rate).execute }

  let(:create_exchange_rate) { instance_double("CreateExchangeRate") }

  context "when given valid data" do
    let(:data) { valid_mapped_data }

    before do
      create_records(data)
    end

    it "calls create and execute exchange rate service 4 times" do
      expect(create_exchange_rate).to receive(:new).with(matching_mapped_hash(data)).exactly(4).times.and_return(create_exchange_rate)
      expect(create_exchange_rate).to receive(:invalid?).exactly(4).times.and_return(false)
      expect(create_exchange_rate).to receive(:execute).exactly(4).times.and_return(create_exchange_rate)
      create_exchange_rates
    end
  end

  context "when given partially valid data" do
    let(:data) { partially_valid_mapped_data }

    before do
      create_records(data)
    end

    it "will only attempt to create valid records" do
      expect(create_exchange_rate).to receive(:new).with(matching_mapped_hash(data)).exactly(2).times.and_return(create_exchange_rate)
      expect(create_exchange_rate).to receive(:invalid?).exactly(2).times.and_return(false)
      expect(create_exchange_rate).to receive(:execute).exactly(2).times.and_return(create_exchange_rate)
      create_exchange_rates
    end
  end

  context "when given invalid mapped data" do
    context "when the from currency is invalid" do
      let(:data) do
        [
          { :bank=>"anz", :from_currency=>"Mike", :to_currency=>"AUD", :rate=>"0.9188", :recorded_at=>Time.now.change(:sec => 0) }
        ]
      end

      it "does not try and call CreateExchangeRate command" do
        expect(create_exchange_rate).to receive(:execute).exactly(0).times
        create_exchange_rates
      end
    end

    context "when the to currency is invalid" do
      let(:data) do
        [
          { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"Mike", :rate=>"0.9188", :recorded_at=>Time.now.change(:sec => 0) }
        ]
      end

      it "does not try and call CreateExchangeRate command" do
        expect(create_exchange_rate).to receive(:execute).exactly(0).times
        create_exchange_rates
      end
    end

    context "when the bank name is invalid" do
      let(:data) do
        [
          { :bank=>"mike", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"0.9188", :recorded_at=>Time.now.change(:sec => 0) }
        ]
      end

      it "does not try and call CreateExchangeRate command" do
        expect(create_exchange_rate).to receive(:execute).exactly(0).times
        create_exchange_rates
      end
    end

    context "when the rate is empty" do
      let(:data) do
        [
          { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"", :recorded_at=>Time.now.change(:sec => 0) }
        ]
      end

      it "does not try and call CreateExchangeRate command" do
        expect(create_exchange_rate).to receive(:execute).exactly(0).times
        create_exchange_rates
      end
    end

    context "when the recorded_at is empty" do
      let(:data) do
        [
          { :bank=>"anz", :from_currency=>"NZD", :to_currency=>"AUD", :rate=>"1.32", :recorded_at=>"" }
        ]
      end

      it "does not try and call CreateExchangeRate command" do
        expect(create_exchange_rate).to receive(:execute).exactly(0).times
        create_exchange_rates
      end
    end
  end
end
